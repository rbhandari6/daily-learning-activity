function solution(A)
{
    let n = A.length;
    // To mark the occurrence of elements
    let present = new Array(n+1);
     
     
    for(let i=0;i<n+1;i++)
    {
        present[i]=false;
    }
    // Mark the occurrences
    for (let i = 0; i < n; i++)
    {
        if (A[i] > 0 && A[i] <= n)
        {
            present[A[i]] = true;
        }
    }
    // Find the first element which didn't
    // appear in the original array

    for (let i = 1; i <= n; i++)
    {
        if (!present[i])
        {
            return i;
        }
    }
    // If the original array was of the type {1, 2, 3} in its sorted form then we will 
    return n + 1;
}
 
let arr = [1, 10, 2, -10, -20,3,4]
console.log(solution(arr));
 