let arr = [5, 5, 10, 10, 10, 5, 10];
let N = arr.length;
console.log(findMaxSum(arr, N));

function findMaxSum(arr, N) {
    let Sumadjcarr = new Array(N);    // lets declare the Sumadjcarr array
    for (let i = 0; i < N; i++) {
        Sumadjcarr[i] = new Array(2);
    }
    if (N == 1) {
        return arr[0];
    }

    Sumadjcarr[0][0] = 0; // This is to initialize the values in Sumadjcarr array
    Sumadjcarr[0][1] = arr[0];

    for (let i = 1; i < N; i++) {
        Sumadjcarr[i][1] = Sumadjcarr[i - 1][0] + arr[i];
        Sumadjcarr[i][0] = Math.max(Sumadjcarr[i - 1][1],
            Sumadjcarr[i - 1][0]);
        // this loop for finding maximum possible sum
    }

    // here we will return the maximum sum
    return Math.max(Sumadjcarr[N - 1][0], Sumadjcarr[N - 1][1]);
}




